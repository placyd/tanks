package com.pawegio.sniadanie.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.pawegio.sniadanie.MyGdxGame;

/**
 * Created by Michał on 2014-12-05.
 */
public class FinishScreen implements Screen {

    private final SpriteBatch batch;
    private final Texture bg;
    private Stage stage;
    private Table table = new Table();

    private OrthographicCamera cam;

    private Skin skin = new Skin(Gdx.files.internal("ui/uiskin.json"));

    private TextButton buttonPlay = new TextButton("Graj ponownie", skin);
    private TextButton buttonExit = new TextButton("Exit", skin);

    private Label title = new Label("Sniadanie z Connectmedica",skin);

    public FinishScreen( BattleFieldScreen.GameState gameState) {
        this.stage = new Stage(new FitViewport(800, 480));
        batch = new SpriteBatch();
        bg = new Texture("bg.jpg");

        if(gameState == BattleFieldScreen.GameState.DISCONNECT){
            title.setText("Zostales rozlaczony :(");
        }else if(gameState == BattleFieldScreen.GameState.WIN){
            title.setText("Wygrales :)");
        }else if(gameState == BattleFieldScreen.GameState.LOSE){
            title.setText("Przegrales :(");
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        batch.setProjectionMatrix(cam.combined);

        batch.begin();
        batch.draw(bg, 0, 0, 800, 480);
        batch.end();


        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

        cam = new OrthographicCamera();
        cam.setToOrtho(true, 800, 480);


        buttonPlay.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game)Gdx.app.getApplicationListener()).setScreen(new AddNameScreen());
            }
        });
        buttonExit.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
                // or System.exit(0);
            }
        });
        //The elements are displayed in the order you add them.
        //The first appear on top, the last at the bottom.
        table.add(title).padBottom(40).row();
        table.add(buttonPlay).size(cam.viewportWidth / 2, cam.viewportHeight / 4).padBottom(20).row();
        table.add(buttonExit).size(cam.viewportWidth / 2, cam.viewportHeight / 4).padBottom(20).row();

        table.setFillParent(true);
        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
