package com.pawegio.sniadanie.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.pawegio.sniadanie.Bullet;
import com.pawegio.sniadanie.Tank;
import com.pawegio.sniadanie.appwarp.WarpController;
import com.pawegio.sniadanie.appwarp.WarpListener;
import org.json.JSONObject;

/**
 * Created by Michał on 2014-12-04.
 */
public class BattleFieldScreen implements Screen, WarpListener {


    final int SPEED = 3;

    OrthographicCamera camera;
    Stage stage;
    SpriteBatch batch;

    // Field
    Texture bg;

    // Bullet
    TextureRegion bulletTexture;

    // Tank
    TextureRegion tankTexture;
    Tank tank;

    // EnemyTank
    TextureRegion enemyTankTexture;
    Tank enemyTank;

    // Touchpad
    Touchpad touchpad;
    Touchpad.TouchpadStyle touchpadStyle;
    Skin touchpadSkin;

    // Fire
    Texture fireTexture;

    // AppWarp
    WarpController warpController;
    int counter = 0;

    // Sound
    private Sound sound;
    boolean isPlaying = false;
    private int impactForce = 60;
    private Skin skin;
    private Dialog dialog;

    private GameState currentState;

    //Health belt
    private ShapeRenderer shapeRenderer;

    private void onStop(){
        sound.stop();
    }


    @Override
    public void dispose() {
        bg.dispose();
        sound.dispose();
        skin.dispose();
        batch.dispose();
        stage.dispose();
    }

    public enum GameState {
        WAITING, RUNNING, WIN, LOSE, DISCONNECT
    }


    public BattleFieldScreen(String userName) {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        setTouchpad();

        stage = new Stage(new FitViewport(800, 480));
        stage.addActor(touchpad);
        Gdx.input.setInputProcessor(stage);

        sound = Gdx.audio.newSound(Gdx.files.internal("tank.mp3"));

        bg = new Texture("bg.jpg");

        skin = new Skin(Gdx.files.internal("ui/uiskin.json"));

        bulletTexture = new TextureRegion(new Texture("bullet.png"));

        tankTexture = new TextureRegion(new Texture("tank.png"));
        tank = new Tank(new Vector2(150, 150), tankTexture.getRegionHeight(), tankTexture.getRegionWidth());
        tank.rotation = 180;

        enemyTankTexture = new TextureRegion(new Texture("enemyTank.png"));
        enemyTank = new Tank(new Vector2(camera.viewportWidth - enemyTankTexture.getRegionWidth() - 150,
                camera.viewportHeight - enemyTankTexture.getRegionHeight() - 150), enemyTankTexture.getRegionHeight(), enemyTankTexture.getRegionWidth());

        fireTexture = new Texture("fire.png");

        warpController = WarpController.getInstance();
        warpController.setListener(this);
        warpController.startApp(userName);

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);


        if (currentState == GameState.RUNNING) {

            if (tank.getLife() <= 0) {
                int tem1 = tank.getLife();
                int tem2 = enemyTank.getLife();
                warpController.updateResult(WarpController.GAME_LOOSE, "przegrales");
            } else if (enemyTank.getLife() <= 0) {
                warpController.updateResult(WarpController.GAME_WIN, "wygrales");
            }


            tank.update();
            enemyTank.update();


            if (collisionWithEnemyDetect()) {

                tank.setLife(tank.getLife() - 10);
                enemyTank.setLife(enemyTank.getLife() - 10);

                if (tank.position.x > enemyTank.position.x) {
                    tank.position.x = tank.position.x + impactForce;
                } else {
                    tank.position.x = tank.position.x - impactForce;
                }
                if (tank.position.y > enemyTank.position.y) {
                    tank.position.y = tank.position.y + impactForce;
                } else {
                    tank.position.y = tank.position.y - impactForce;
                }
            }

            if (Gdx.input.justTouched() && isFireTouched()) {
                Vector2 position = new Vector2(tank.position)
                        .add(tankTexture.getRegionWidth() / 2,
                                tankTexture.getRegionHeight() / 2);
                tank.bullet = new Bullet(position, tank.rotation);
                tank.bullet.visible = true;
            }

            if (tank.bullet.visible) {
                tank.bullet.position.x += -Math.cos(Math.toRadians(tank.bullet.rotation)) * 7;
                tank.bullet.position.y += -Math.sin(Math.toRadians(tank.bullet.rotation)) * 7;
                if (isEnemyAimed()) {
                    enemyTank.setLife(enemyTank.getLife() - 20);
                }
            }

            if (touchpad.isTouched()) {
                if (!isPlaying) {
                    sound.loop();
                    isPlaying = true;
                }
                tank.position.x += touchpad.getKnobPercentX() * SPEED;
                tank.position.y += touchpad.getKnobPercentY() * SPEED;
                float angle = (new Vector2(0, 0)).sub(touchpad.getKnobPercentX(), touchpad.getKnobPercentY()).angle();
                tank.rotation = angle;
            } else {
                if (isPlaying) {
                    sound.stop();
                    isPlaying = false;
                }
            }

            // Wysyła update do App Warp jeśli jest ruch
            if ((touchpad.getKnobPercentX() != 0 || touchpad.getKnobPercentY() != 0)
                    && counter % 10 == 0) {
                sendLocation();
                counter++;
            } else if (touchpad.isTouched()) {
                counter++;
            }

            batch.begin();
            batch.draw(bg, 0, 0, camera.viewportWidth, camera.viewportHeight);
            batch.draw(tankTexture, tank.position.x, tank.position.y,
                    tankTexture.getRegionWidth() / 2, tankTexture.getRegionHeight() / 2,
                    tankTexture.getRegionWidth(), tankTexture.getRegionHeight(),
                    1, 1, tank.rotation);
            batch.draw(enemyTankTexture, enemyTank.position.x, enemyTank.position.y,
                    enemyTankTexture.getRegionWidth() / 2, enemyTankTexture.getRegionHeight() / 2,
                    enemyTankTexture.getRegionWidth(), enemyTankTexture.getRegionHeight(),
                    1, 1, enemyTank.rotation);
            if (tank.bullet.visible) {
                batch.draw(bulletTexture, tank.bullet.position.x, tank.bullet.position.y,
                        bulletTexture.getRegionWidth() / 2, bulletTexture.getRegionHeight() / 2,
                        bulletTexture.getRegionWidth(), bulletTexture.getRegionHeight(),
                        1, 1, tank.bullet.rotation);
            }
            batch.draw(fireTexture, camera.viewportWidth - camera.viewportWidth / 8 - camera.viewportWidth / 20,
                    camera.viewportWidth / 20, camera.viewportWidth / 8, camera.viewportWidth / 8);
            batch.end();

            updateHealthBelt(tank.getLife());
        }

        stage.act();
        stage.draw();
    }

    private boolean collisionWithEnemyDetect() {
        return Intersector.overlaps(tank.getBoundingCircle(), enemyTank.getBoundingCircle());
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        //    Gdx.input.setInputProcessor(stage = new Stage());

        currentState = GameState.WAITING;

        dialog = new Dialog("", skin) {

            {
                text("Czekaj na gracza");
            }

        };

        dialog.setSize(500, 300);
        dialog.show(stage);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void onWaitingStarted(String message) {
        Gdx.app.log("AppWarp", message);
    }

    @Override
    public void onError(String message) {
        Gdx.app.log("AppWarp", message);
    }

    @Override
    public void onGameStarted(String message) {
        Gdx.app.log("AppWarp", message);
        dialog.hide();

        currentState = GameState.RUNNING;
    }

    @Override
    public void onGameFinished(int code, boolean isRemote) {
        Gdx.app.log("AppWarp", "finish");

        if (currentState == GameState.RUNNING) {
            if (code == WarpController.ENEMY_LEFT) {

                currentState = GameState.DISCONNECT;
                onStop();
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {

                        warpController.handleLeave();
                        Gdx.app.log("tank life: ", ""+tank.getLife());
                        Gdx.app.log("enemy tank life: ", ""+enemyTank.getLife());
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new FinishScreen(currentState));
                    }
                });

            } else if (code == WarpController.GAME_LOOSE) {

                currentState = GameState.LOSE;
                onStop();

                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        warpController.handleLeave();
                        Gdx.app.log("tank life: ", "" + tank.getLife());
                        Gdx.app.log("enemy tank life: ", "" + enemyTank.getLife());
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new FinishScreen(currentState));
                    }
                });
            } else if (code == WarpController.GAME_WIN) {

                currentState = GameState.WIN;
                onStop();
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        warpController.handleLeave();
                        Gdx.app.log("tank life: ", "" + tank.getLife());
                        Gdx.app.log("enemy tank life: ", "" + enemyTank.getLife());
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new FinishScreen(currentState));
                    }
                });

            }
        }
    }

    @Override
    public void onGameUpdateReceived(String message) {
        Gdx.app.log("AppWarp", message);
        try {
            JSONObject data = new JSONObject(message);
            float x = (float) data.getDouble("x");
            float y = (float) data.getDouble("y");
            float rotation = (float) data.getDouble("rotation");
            updateEnemyLocation(x, y, rotation);
        } catch (Exception e) {

        }
    }

    private void sendLocation() {
        try {
            JSONObject data = new JSONObject();
            data.put("x", tank.position.x);
            data.put("y", tank.position.y);
            data.put("rotation", tank.rotation);
            warpController.sendGameUpdate(data.toString());
            Gdx.app.log("Game", "location sent");
        } catch (Exception e) {

        }
    }

    private void setTouchpad() {
        touchpadSkin = new Skin();
        touchpadSkin.add("touchBackground", new Texture("touchBackground.png"));
        touchpadSkin.add("touchKnob", new Texture("touchKnob.png"));

        touchpadStyle = new Touchpad.TouchpadStyle();
        touchpadStyle.background = touchpadSkin.getDrawable("touchBackground");
        touchpadStyle.knob = touchpadSkin.getDrawable("touchKnob");

        touchpad = new Touchpad(10, touchpadStyle);
        touchpad.setBounds(camera.viewportWidth / 20, camera.viewportWidth / 20,
                camera.viewportWidth / 8, camera.viewportWidth / 8);
    }

    private void updateEnemyLocation(float x, float y, float rotation) {
        enemyTank.position.x = x;
        enemyTank.position.y = y;
        enemyTank.rotation = rotation;
    }

    private boolean isFireTouched() {
        Circle circle = new Circle(
                camera.viewportWidth - camera.viewportWidth / 8 - camera.viewportWidth / 20 + fireTexture.getWidth() / 2,
                camera.viewportHeight - camera.viewportWidth / 20 - fireTexture.getHeight() / 2,
                fireTexture.getWidth() / 2);
        int x = Gdx.input.getX();
        int y = Gdx.input.getY();
        return circle.contains(x, y);
    }

    private void updateHealthBelt(int health) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1, 0, 0, 1);
        int length = 200;

        shapeRenderer.rect(10, 440, ((health * length) / Tank.MAX_LIVE), 30);
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private boolean isEnemyAimed() {
        float x = tank.bullet.position.x;
        float y = tank.bullet.position.y;
        return enemyTank.getBoundingCircle().contains(x, y);
    }
}
