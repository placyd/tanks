package com.pawegio.sniadanie;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.pawegio.sniadanie.Screens.MenuScreen;


public class MyGdxGame extends Game {


    @Override
    public void create() {

        setScreen(new MenuScreen());
    }


    @Override
    public void dispose() {
        super.dispose();

    }

}
