package com.pawegio.sniadanie;

import com.badlogic.gdx.math.Vector2;

/**
 * @author pawegio
 */
public class Bullet {

    public Vector2 position = new Vector2();
    public float rotation;
    public boolean visible;

    public Bullet() {
        this.visible = false;
    }

    public Bullet(Vector2 position, float rotation) {
        this.position = position;
        this.rotation = rotation;
        this.visible = true;
    }
}
