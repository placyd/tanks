package com.pawegio.sniadanie;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

/**
 * @author pawegio
 */
public class Tank {

    public Vector2 position = new Vector2();
    public float rotation;
    private int life, width, height;
    public Bullet bullet = new Bullet();
    public static final int MAX_LIVE = 50;


    private Circle boundingCircle;

    public Tank(Vector2 position, int height, int width) {
        this.position = position;
        this.rotation = 0;
        this.height = height;
        this.width = width;
        life = MAX_LIVE;
        boundingCircle = new Circle(position, width / 2);

    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public void update(){
        boundingCircle.set(this.position, width / 2 );
    }


    public Circle getBoundingCircle() {
        return boundingCircle;
    }

}
